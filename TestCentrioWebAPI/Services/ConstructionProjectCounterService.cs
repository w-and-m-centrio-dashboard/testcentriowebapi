﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TestCentrioWebAPI.Models;

namespace TestCentrioWebAPI.Services
{
    public class ConstructionProjectPeriodCounterService : CentrioServiceBase
    {
        public ConstructionProjectPeriodCounterService(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<List<ConstructionProjectPeriodCounter>> GetConstructionProjectPeriodCounterAsync()
        {
            List<ConstructionProjectPeriodCounter> projects = new List<ConstructionProjectPeriodCounter>();
            SqlConnection sqlConnection = new SqlConnection(GetCentrioConnectionString());
            sqlConnection.Open();
            string sqlStatement = "select cast(year(tcAttCreated) as nvarchar(4)) + '/' + right('0' + cast(month(tcAttCreated) as nvarchar(2)),2) as 'Jahr-Monat', replace(tclinClassID,'clsConPro','Projekte') as Klasse, COUNT(*) as Anzahl from tbDConstructionProjects group by cast(year(tcAttCreated) as nvarchar(4)) + '/' + right('0' + cast(month(tcAttCreated) as nvarchar(2)),2), tclinClassID ";
            SqlCommand sqlCommand = new SqlCommand(sqlStatement, sqlConnection);
            SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();
            while (sqlDataReader.Read())
            {
                ConstructionProjectPeriodCounter project = new ConstructionProjectPeriodCounter();
                project.Period = (string)sqlDataReader["Jahr-Monat"];
                project.Counter = (int)sqlDataReader["Anzahl"];
                projects.Add(project);
            }
            sqlDataReader.Close();
            sqlConnection.Close();
            return projects;
        }

        public async Task<List<ProjectPeriodCounter>> GetProjectPeriodCounterAsync()
        {
            List<ProjectPeriodCounter> projects = new List<ProjectPeriodCounter>();
            SqlConnection sqlConnection = new SqlConnection(GetCentrioConnectionString());
            sqlConnection.Open();
            string sqlStatement = @"SELECT DISTINCT prjp.Period,  prjp.Periode2, Count(*) as 'Counter'
                                        FROM( SELECT DISTINCT    cp.attConProReferenceNumber  AS ProjektNr ,
                                        cast(year(d.tcAttCreated) as nvarchar(4)) + right('0' + cast(month(d.tcAttCreated) as nvarchar(2)),2) AS Periode2,
								        CONVERT(CHAR(4), d.tcAttCreated, 100) + cast(year(d.tcAttCreated) as nvarchar(4)) AS Period
                                                 FROM centrioData_Prod.dbo.tbLStrucNodeDoc AS l INNER JOIN
                                                 centrioData_Prod.dbo.tbDStructureNodes AS sn ON l.tcLinParentObjectID = sn.tcSysObjectID INNER JOIN
                                                 centrioData_Prod.dbo.tbDConstructionProjects AS cp ON sn.tcLinStructureMasterObjectID = cp.tcSysObjectID AND       
                                                 sn.tcLinStructureMasterClassID = 'clsConPro' INNER JOIN
	                                             centrioData_Prod.dbo.tbDDocuments as d on l.tcLinChildObjectID = d.tcSysObjectID INNER JOIN
	                                             centrioData_Prod.dbo.tbDInternalUnits as u on cp.linConProResponsibleIntUnit = u.tcSysObjectID
								                 WHERE ( NOT d.tcAttFileSize IS NULL )) as prjp
							GROUP BY  prjp.Periode2,prjp.Period
							order by prjp.Periode2";
            SqlCommand sqlCommand = new SqlCommand(sqlStatement, sqlConnection);
            SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();
            while (sqlDataReader.Read())
            {
                ProjectPeriodCounter project = new ProjectPeriodCounter();
                project.Period = (string)sqlDataReader["Period"];
                project.Counter = (int)sqlDataReader["Counter"];
                projects.Add(project);
            }
            sqlDataReader.Close();
            sqlConnection.Close();
            return projects;
        }

        public async Task<List<FileSizeDivisionCounter>> GetFileSizeDivisionCounterAsync()
        {
            List<FileSizeDivisionCounter> projects = new List<FileSizeDivisionCounter>();
            SqlConnection sqlConnection = new SqlConnection(GetCentrioConnectionString());
            sqlConnection.Open();
            string sqlStatement = @"select fs.dist_group as 'Range(MB)', count(*) as Counter
from
(
 select case when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END)  between 0  and 10000 then '0 - 10000'
  when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END)  between 10000 and 20000 then '10000 - 20000'
  when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END)  between 20000 and 30000 then '20000 - 30000'
  when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END)  between 30000 and 40000 then '30000 - 40000'
  when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END)  between 40000 and 50000 then '40000 - 50000'
				when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END)  between 50000  and 75000 then '50000 - 75000'
				when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END)  between 75000  and 100000 then '75000 - 100000'
             when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END) between 100000 and 200000 then '100000 - 200000'            
             when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END) between 200000 and 300000 then '200000 - 300000' 
			    when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END) between 300000 and 400000 then '300000 - 400000' 
  when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END) between 400000 and 500000 then '400000 - 500000' 
  when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END) between 500000 and 600000 then '500000 - 600000' 
  when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END) between 600000 and 700000 then '600000 - 700000' 
  when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END) between 700000 and 800000 then '700000 - 800000' 
  when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END) between 800000 and 900000 then '800000 - 900000' 
    when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END) between 900000 and 1000000 then '900000 - 1000000' 
  when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END) between 1000000 and 1500000 then '1000000 - 1500000' 	
    when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END) between 1500000 and 2000000 then '1500000 - 2000000' 	
   when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END) between 2000000 and 3000000 then '2000000 - 3000000' 	
    when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END) between 3000000 and 4000000 then '3000000 - 4000000' 	
	 when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END) between 4000000 and 5000000 then '4000000 - 5000000' 	
	      when (CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END) between 5000000 and 3000000000 then '5000000 - 3000000000' 
			 end as dist_group
 from(centrioData_Prod.dbo.tbLStrucNodeDoc AS l INNER JOIN
                                centrioData_Prod.dbo.tbDStructureNodes AS sn ON l.tcLinParentObjectID = sn.tcSysObjectID INNER JOIN
                                centrioData_Prod.dbo.tbDConstructionProjects AS cp ON sn.tcLinStructureMasterObjectID = cp.tcSysObjectID AND       
                                                                                      sn.tcLinStructureMasterClassID = 'clsConPro' INNER JOIN
	                            centrioData_Prod.dbo.tbDDocuments as d on l.tcLinChildObjectID = d.tcSysObjectID INNER JOIN
	                            centrioData_Prod.dbo.tbDInternalUnits as u on cp.linConProResponsibleIntUnit = u.tcSysObjectID) WHERE ( NOT d.tcAttFileSize IS NULL ) ) as fs
							
group by fs.dist_group
order by fs.dist_group	
";
            SqlCommand sqlCommand = new SqlCommand(sqlStatement, sqlConnection);
            SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();
            while (sqlDataReader.Read())
            {
                FileSizeDivisionCounter project = new FileSizeDivisionCounter();
                //project.Division = (string)sqlDataReader["Division"] + "-"+ (Int32.Parse( (string)sqlDataReader["Period"])+2000000).ToString() ;               

               // project.DivisionField = (string)sqlDataReader["Range(MB)"];
                project.Division = Int32.Parse(((string)sqlDataReader["Range(MB)"]).Split(' ')[0]);
                project.Counter = (int)sqlDataReader["Counter"];
                projects.Add(project);
            }
            sqlDataReader.Close();
            sqlConnection.Close();
            return projects;
        }
    }
}
