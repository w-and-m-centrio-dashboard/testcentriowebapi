﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TestCentrioWebAPI.Models;

namespace TestCentrioWebAPI.Services
{
    public class DocumentCategoryCounterService : CentrioServiceBase
    {


        public DocumentCategoryCounterService(IConfiguration configuration) : base(configuration)
        {
        }


        public async Task<List<DocumentCategoryCounter>> GetDocumentCategoryCounterAsync()
        {
            List<DocumentCategoryCounter> documentCategories = new List<DocumentCategoryCounter>();
            SqlConnection sqlConnection = new SqlConnection(GetCentrioConnectionString());
            sqlConnection.Open();
            string sqlStatement = @"SELECT d.tcLinCategoryID AS KategorieID
	                                ,d.tcLinSyncSystemID AS SyncSystemID
                                    ,w.attText AS Category
                                    ,COUNT([tcLinCategoryID]) AS Anzahl
                            FROM [centrioData_Prod].[dbo].[tbDDocuments] AS d INNER JOIN
                                [centrioConfig_Prod].[config].[tbCModuleCategoryCategories] AS c ON d.tcLinCategoryID = c.sysCategoryID LEFT OUTER JOIN
	                            [centrioConfig_Prod].[config].[tbCWordbook] AS w ON w.sysWordbookKey = SUBSTRING(c.attCaption, 11, LEN(c.attCaption) - 11)
	                        WHERE (w.linUILanguageID = 'de') OR (w.linUILanguageID IS NULL)
                            GROUP BY d.tcLinCategoryID, w.attText, d.tcLinSyncSystemID ORDER BY Anzahl DESC";
            SqlCommand sqlCommand = new SqlCommand(sqlStatement, sqlConnection);
            SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

            while (sqlDataReader.Read())
            {
                DocumentCategoryCounter doccat = new DocumentCategoryCounter();
                doccat.Category = (string)sqlDataReader["Category"];
                doccat.Counter = (int)sqlDataReader["Anzahl"];
                documentCategories.Add(doccat);
               

            }
            sqlDataReader.Close();
            sqlConnection.Close();
            return documentCategories;
        }

        public async Task<List<DocumentSystemCounter>> GetDocumentSystemCounterAsync()
        {
            List<DocumentSystemCounter> documentCategories = new List<DocumentSystemCounter>();
            SqlConnection sqlConnection = new SqlConnection(GetCentrioConnectionString());
            sqlConnection.Open();
            string sqlStatement = @"SELECT DISTINCT  ISNULL(doccat.SyncSystemID, 'Centrio') AS Field, sum(doccat.Anzahl) as 'Counter'
                                FROM(
                            SELECT 	 d.tcLinSyncSystemID AS SyncSystemID ,COUNT([tcLinCategoryID]) AS Anzahl
                            FROM [centrioData_Prod].[dbo].[tbDDocuments] AS d INNER JOIN
                                [centrioConfig_Prod].[config].[tbCModuleCategoryCategories] AS c ON d.tcLinCategoryID = c.sysCategoryID LEFT OUTER JOIN
	                            [centrioConfig_Prod].[config].[tbCWordbook] AS w ON w.sysWordbookKey = SUBSTRING(c.attCaption, 11, LEN(c.attCaption) - 11)
	                        WHERE (w.linUILanguageID = 'de') OR (w.linUILanguageID IS NULL)
                            GROUP BY d.tcLinCategoryID, w.attText, d.tcLinSyncSystemID ) as doccat
						    GROUP BY doccat.SyncSystemID";
            SqlCommand sqlCommand = new SqlCommand(sqlStatement, sqlConnection);
            SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

            while (sqlDataReader.Read())
            {
                DocumentSystemCounter doccat = new DocumentSystemCounter();
                doccat.Category = (string)sqlDataReader["Field"];
                doccat.Counter = (int)sqlDataReader["Counter"];
                documentCategories.Add(doccat);
            }
            sqlDataReader.Close();
            sqlConnection.Close();
            return documentCategories;
        }
    }
}
