﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TestCentrioWebAPI.Models;

namespace TestCentrioWebAPI.Services
{
    public class DocumentProjectService : CentrioServiceBase
    {

        public DocumentProjectService(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<List<DocumentProject>> GetAsync()
        {
            List<DocumentProject> documents = new List<DocumentProject>();
            SqlConnection sqlConnection = new SqlConnection(GetCentrioConnectionString());
            sqlConnection.Open();
            string sqlStatement = @"SELECT DISTINCT 
                                l.tcLinChildObjectID AS DocumentID, 
                                cp.tcSysObjectID AS ParentID, 
                                cp.attConProReferenceNumber AS ProjektNr,
                                u.attIntUnitSapBukrs AS SAPBukrs,
                                u.attIntUnitSapGsbr AS SAPGsber,
                                u.attIntUnitName AS GsberName,
                                ISNULL(cp.linConProSparte, '') AS Sparte,
                                ISNULL(cp.attConProProjektKategorie, '') AS Projektkategorie,
                                ISNULL(cp.attConProSegment, '') AS Segment,
                                ISNULL(cp.attConProBranche, '') AS Branche,
                                ISNULL(cp.attConProAuftragsart, '') AS Auftragsart,
                                ISNULL(cp.attConProAbrechnungsart, '') AS Abrechnungsart,
                                SUBSTRING(cp.attConProReferenceNumber, 5, 2) AS ProjektTyp,
                                cast(year(d.tcAttCreated) as nvarchar(4)) + right('0' + cast(month(d.tcAttCreated) as nvarchar(2)),2) AS Periode,
                                CASE WHEN d.tcAttFileSize IS NULL THEN 0 ELSE d.tcAttFileSize END AS DateiGroesse
                            FROM centrioData_Prod.dbo.tbLStrucNodeDoc AS l INNER JOIN
                                centrioData_Prod.dbo.tbDStructureNodes AS sn ON l.tcLinParentObjectID = sn.tcSysObjectID INNER JOIN
                                centrioData_Prod.dbo.tbDConstructionProjects AS cp ON sn.tcLinStructureMasterObjectID = cp.tcSysObjectID AND       
                                                                                      sn.tcLinStructureMasterClassID = 'clsConPro' INNER JOIN
	                            centrioData_Prod.dbo.tbDDocuments as d on l.tcLinChildObjectID = d.tcSysObjectID INNER JOIN
	                            centrioData_Prod.dbo.tbDInternalUnits as u on cp.linConProResponsibleIntUnit = u.tcSysObjectID
	                            WHERE ( NOT d.tcAttFileSize IS NULL ) ";
            SqlCommand sqlCommand = new SqlCommand(sqlStatement, sqlConnection);
            SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();
            int cnt = 0;
            while (sqlDataReader.Read())
            {
                DocumentProject document = new DocumentProject();
                document.DocumentID = (long)sqlDataReader["DocumentID"];
                document.ParentID = (long)sqlDataReader["ParentID"];
                document.Periode = (string)sqlDataReader["Periode"];
                document.ProjektNr = (string)sqlDataReader["ProjektNr"];
                document.ProjektTyp = (string)sqlDataReader["ProjektTyp"];
                document.SAPBukrs = (string)sqlDataReader["SAPBukrs"];
                document.SAPGsber = (string)sqlDataReader["SAPGsber"];
                document.GsberName = (string)sqlDataReader["GsberName"];
                document.Projektkategorie = (string)sqlDataReader["Projektkategorie"];
                document.Segment = (string)sqlDataReader["Segment"];
                document.Abrechnungsart = (string)sqlDataReader["Abrechnungsart"];
                document.Auftragsart = (string)sqlDataReader["Auftragsart"];
                document.Branche = (string)sqlDataReader["Branche"];
                document.DateiGroesse = (long)sqlDataReader["DateiGroesse"];
                documents.Add(document);
                cnt++;
            }
            sqlDataReader.Close();
            sqlConnection.Close();
            return documents;
        }
    }
}
