﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCentrioWebAPI.Services
{
    public class CentrioServiceBase
    {
        private string CentrioConnectionString { get; set; } = string.Empty;
        private string CentrioUserId { get; set; } = string.Empty;
        private string CentrioUserPassword { get; set; } = string.Empty;


        public CentrioServiceBase(IConfiguration configuration)
        {
            CentrioConnectionString = configuration.GetConnectionString("CentrioDataProd");
            var centriologinSection = configuration.GetSection("centriologin");
            CentrioUserId = centriologinSection.GetChildren().Where(c => c.Key == "userid").FirstOrDefault().Value;
            CentrioUserPassword = centriologinSection.GetChildren().Where(c => c.Key == "password").FirstOrDefault().Value;
            CentrioConnectionString = CentrioConnectionString + ";User Id=" + CentrioUserId + ";Password=" + CentrioUserPassword;
        }

        public string GetCentrioConnectionString()
        {
            return CentrioConnectionString;
        }
    }
}
