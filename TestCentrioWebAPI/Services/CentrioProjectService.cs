﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TestCentrioWebAPI.Models;

namespace TestCentrioWebAPI.Services
{
    public class CentrioProjectService : CentrioServiceBase
    {
        public CentrioProjectService(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<List<CentrioProject>> GetCentrioProjectsAsync()
        {
            List<CentrioProject> projects = new List<CentrioProject>();
            SqlConnection sqlConnection = new SqlConnection(GetCentrioConnectionString());
            sqlConnection.Open();
            string sqlStatement = "SELECT tcSysObjectID, attConProReferenceNumber, attConProName FROM tbDConstructionProjects";
            SqlCommand sqlCommand = new SqlCommand(sqlStatement, sqlConnection);
            SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();
            while (sqlDataReader.Read())
            {
                CentrioProject project = new CentrioProject();
                project.SysObjectId = (long)sqlDataReader["tcSysObjectId"];
                project.ProjectNumber = (string)sqlDataReader["attConProReferenceNumber"];
                project.ProjectName = (string)sqlDataReader["attConProName"];
                projects.Add(project);
            }
            sqlDataReader.Close();
            sqlConnection.Close();
            return projects;
        }
    }
}
