﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TestCentrioWebAPI.Models;

namespace TestCentrioWebAPI.Services
{
    public class DocumentClassCounterService : CentrioServiceBase
    {
        public DocumentClassCounterService(IConfiguration configuration) : base(configuration)
        {
        }



        public async Task<List<DocumentPeriodCounter>> GetDocumentPeriodAsync()
        {
            List<DocumentPeriodCounter> projects = new List<DocumentPeriodCounter>();
            SqlConnection sqlConnection = new SqlConnection(GetCentrioConnectionString());
            sqlConnection.Open();
            string sqlStatement = "select cast(year(tcAttCreated) as nvarchar(4)) + '/' + right('0' + cast(month(tcAttCreated) as nvarchar(2)),2) as 'Jahr-Monat', replace(tclinClassID,'clsDoc','Dokumente') as Klasse , COUNT(*) as Anzahl from tbDDocuments group by cast(year(tcAttCreated) as nvarchar(4)) + '/' + right('0' + cast(month(tcAttCreated) as nvarchar(2)),2), tclinClassID ";
            SqlCommand sqlCommand = new SqlCommand(sqlStatement, sqlConnection);
            SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();
            while (sqlDataReader.Read())
            {
                DocumentPeriodCounter project = new DocumentPeriodCounter();
                project.Period = (string)sqlDataReader["Jahr-Monat"];
                project.Counter = (int)sqlDataReader["Anzahl"];
                projects.Add(project);
            }
            sqlDataReader.Close();
            sqlConnection.Close();
            return projects;
        }


        public async Task<List<DocumentCompanyCounter>> GetDocumentCompanyAsync()
        {
            List<DocumentCompanyCounter> projects = new List<DocumentCompanyCounter>();
            SqlConnection sqlConnection = new SqlConnection(GetCentrioConnectionString());
            sqlConnection.Open();
            string sqlStatement = @"SELECT DISTINCT 
                                    CASE u.attIntUnitSapBukrs
                                    WHEN   2500 THEN 'TSB'
                                    WHEN  3300 THEN 'WM-GS'
                                    WHEN 3700 THEN 'Fischer'
                                    WHEN 4500 THEN 'PST'
                                    WHEN  4800 THEN 'Ing.Bau'
                                    WHEN  8300 THEN 'HIB'
                                    END AS [u.attIntUnitSapBukrs], Count(*) as 'Counter'
                            FROM centrioData_Prod.dbo.tbLStrucNodeDoc AS l INNER JOIN
                                centrioData_Prod.dbo.tbDStructureNodes AS sn ON l.tcLinParentObjectID = sn.tcSysObjectID INNER JOIN
                                centrioData_Prod.dbo.tbDConstructionProjects AS cp ON sn.tcLinStructureMasterObjectID = cp.tcSysObjectID AND       
                                                                                      sn.tcLinStructureMasterClassID = 'clsConPro' INNER JOIN
	                            centrioData_Prod.dbo.tbDDocuments as d on l.tcLinChildObjectID = d.tcSysObjectID INNER JOIN
	                            centrioData_Prod.dbo.tbDInternalUnits as u on cp.linConProResponsibleIntUnit = u.tcSysObjectID
	                            WHERE ( NOT d.tcAttFileSize IS NULL ) 
								Group by  u.attIntUnitSapBukrs
                                 order by Counter desc
							";
            SqlCommand sqlCommand = new SqlCommand(sqlStatement, sqlConnection);
            SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();
            while (sqlDataReader.Read())
            {
                DocumentCompanyCounter project = new DocumentCompanyCounter();
                project.CompanyCode = (string)sqlDataReader["u.attIntUnitSapBukrs"];
                project.Counter = (int)sqlDataReader["Counter"];
                projects.Add(project);
            }
            sqlDataReader.Close();
            sqlConnection.Close();
            return projects;
        }

        public async Task<List<DocumentBusinessAreaCounter>> GetDocumentBusinessAreaAsync()
        {
            List<DocumentBusinessAreaCounter> businessareas = new List<DocumentBusinessAreaCounter>();
            SqlConnection sqlConnection = new SqlConnection(GetCentrioConnectionString());
            sqlConnection.Open();
            string sqlStatement = @"SELECT DISTINCT  u.attIntUnitSapGsbr AS SAPGsber,
                                    u.attIntUnitName AS GsberName, Count(*) as 'Counter'
                            FROM centrioData_Prod.dbo.tbLStrucNodeDoc AS l INNER JOIN
                                centrioData_Prod.dbo.tbDStructureNodes AS sn ON l.tcLinParentObjectID = sn.tcSysObjectID INNER JOIN
                                centrioData_Prod.dbo.tbDConstructionProjects AS cp ON sn.tcLinStructureMasterObjectID = cp.tcSysObjectID AND       
                                                                                      sn.tcLinStructureMasterClassID = 'clsConPro' INNER JOIN
	                            centrioData_Prod.dbo.tbDDocuments as d on l.tcLinChildObjectID = d.tcSysObjectID INNER JOIN
	                            centrioData_Prod.dbo.tbDInternalUnits as u on cp.linConProResponsibleIntUnit = u.tcSysObjectID
	                            WHERE ( NOT d.tcAttFileSize IS NULL ) 
								Group by  u.attIntUnitName,u.attIntUnitSapGsbr
								order by Counter desc
							";
            SqlCommand sqlCommand = new SqlCommand(sqlStatement, sqlConnection);
            SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();
            while (sqlDataReader.Read())
            {
                DocumentBusinessAreaCounter businessarea = new DocumentBusinessAreaCounter();
                businessarea.BusinessArea = (string)sqlDataReader["SAPGsber"] + '-' + (string)sqlDataReader["GsberName"];
                businessarea.Counter = (int)sqlDataReader["Counter"];
                businessareas.Add(businessarea);
            }
            sqlDataReader.Close();
            sqlConnection.Close();
            return businessareas;
        }

        public async Task<List<DocumentProjectPeriodCounter>> GetDocumentProjectPeriodAsync()
        {
            List<DocumentProjectPeriodCounter> periods = new List<DocumentProjectPeriodCounter>();
            SqlConnection sqlConnection = new SqlConnection(GetCentrioConnectionString());
            sqlConnection.Open();
            string sqlStatement = @"SELECT DISTINCT  CONVERT(CHAR(4), d.tcAttCreated, 100) + cast(year(d.tcAttCreated) as nvarchar(4)) AS Period,cast(year(d.tcAttCreated) as nvarchar(4)) + right('0' + cast(month(d.tcAttCreated) as nvarchar(2)),2) as Period2,Count(*) as 'Counter'
                            FROM centrioData_Prod.dbo.tbLStrucNodeDoc AS l INNER JOIN
                                centrioData_Prod.dbo.tbDStructureNodes AS sn ON l.tcLinParentObjectID = sn.tcSysObjectID INNER JOIN
                                centrioData_Prod.dbo.tbDConstructionProjects AS cp ON sn.tcLinStructureMasterObjectID = cp.tcSysObjectID AND       
                                                                                      sn.tcLinStructureMasterClassID = 'clsConPro' INNER JOIN
	                            centrioData_Prod.dbo.tbDDocuments as d on l.tcLinChildObjectID = d.tcSysObjectID INNER JOIN
	                            centrioData_Prod.dbo.tbDInternalUnits as u on cp.linConProResponsibleIntUnit = u.tcSysObjectID
	                            WHERE ( NOT d.tcAttFileSize IS NULL )
								group by   CONVERT(CHAR(4), d.tcAttCreated, 100) + cast(year(d.tcAttCreated) as nvarchar(4)),cast(year(d.tcAttCreated) as nvarchar(4)) + right('0' + cast(month(d.tcAttCreated) as nvarchar(2)),2)
								ORDER BY  Period2
							";
            SqlCommand sqlCommand = new SqlCommand(sqlStatement, sqlConnection);
            SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();
            while (sqlDataReader.Read())
            {
                DocumentProjectPeriodCounter period = new DocumentProjectPeriodCounter();
                period.Period = (string)sqlDataReader["Period"];
                period.Counter = (int)sqlDataReader["Counter"];
                periods.Add(period);
            }
            sqlDataReader.Close();
            sqlConnection.Close();
            return periods;
        }
    }
}
