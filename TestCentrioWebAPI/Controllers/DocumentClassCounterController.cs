﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCentrioWebAPI.Models;
using TestCentrioWebAPI.Services;

namespace TestCentrioWebAPI.Controllers
{
    [ApiController]
    public class DocumentClassCounterController : ControllerBase
    {
        private DocumentClassCounterService dccs { get; set; }

        public DocumentClassCounterController(IConfiguration configuration)
        {
            dccs = new DocumentClassCounterService(configuration);
        }

        [HttpGet]
        [Route("/api/DocumentPeriodCounter")]
        public async Task<ActionResult<List<DocumentPeriodCounter>>> GetDocumentPeriodAsync()
        {
            try
            {
                List<DocumentPeriodCounter> projects = await dccs.GetDocumentPeriodAsync();
                return Ok(projects);
            }
            catch (Exception e)
            {
                return Problem(detail: e.Message);
            }
        }


        [HttpGet]
        [Route("/api/DocumentCompanyCounter")]
        public async Task<ActionResult<List<DocumentCompanyCounter>>> GetDocumentCompanyAsync()
        {
            try
            {
                List<DocumentCompanyCounter> documentcompanycounters = await dccs.GetDocumentCompanyAsync();
                return Ok(documentcompanycounters);
            }
            catch (Exception e)
            {
                return Problem(detail: e.Message);
            }
        }

        [HttpGet]
        [Route("/api/DocumentBusinessAreaCounter")]
        public async Task<ActionResult<List<DocumentBusinessAreaCounter>>> GetDocumentBusinessAreaAsync()
        {
            try
            {
                List<DocumentBusinessAreaCounter> businessarea = await dccs.GetDocumentBusinessAreaAsync();
                return Ok(businessarea);
            }
            catch (Exception e)
            {
                return Problem(detail: e.Message);
            }
        }

        [HttpGet]
        [Route("/api/DocumentProjectPeriodCounter")]
        public async Task<ActionResult<List<DocumentProjectPeriodCounter>>> GetDocumentProjectPeriodAsync()
        {
            try
            {
                List<DocumentProjectPeriodCounter> period = await dccs.GetDocumentProjectPeriodAsync();
                return Ok(period);
            }
            catch (Exception e)
            {
                return Problem(detail: e.Message);
            }
        }




    }
}
