﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCentrioWebAPI.Models;
using TestCentrioWebAPI.Services;

namespace TestCentrioWebAPI.Controllers
{
    [ApiController]
    public class CentrioProjectController : ControllerBase
    {
        private CentrioProjectService svc { get; set; }

        public CentrioProjectController(IConfiguration configuration)
        {
            svc = new CentrioProjectService(configuration);
        }

        [HttpGet]
        [Route("/api/CentrioProjects")]
        public async Task<ActionResult<List<CentrioProject>>> GetCentrioProjectsAsync()
        {
            try
            {
                List<CentrioProject> projects = await svc.GetCentrioProjectsAsync();
                return Ok(projects);
            }
            catch (Exception e)
            {
                return Problem(detail: e.Message);
            }
        }


    }
}
