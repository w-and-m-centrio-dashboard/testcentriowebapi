﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCentrioWebAPI.Models;
using TestCentrioWebAPI.Services;

namespace TestCentrioWebAPI.Controllers
{
    [ApiController]
    public class ConstructionProjectPeriodCounterController : ControllerBase
    {
        private ConstructionProjectPeriodCounterService cpcs { get; set; }

        public ConstructionProjectPeriodCounterController(IConfiguration configuration)
        {
            cpcs = new ConstructionProjectPeriodCounterService(configuration);
        }

        [HttpGet]
        [Route("/api/ConstructionProjectPeriodCounter")]
        public async Task<ActionResult<List<ConstructionProjectPeriodCounter>>> GetConstructionProjectPeriodCounterAsync()
        {
            try
            {
                List<ConstructionProjectPeriodCounter> projects = await cpcs.GetConstructionProjectPeriodCounterAsync();
                return Ok(projects);
            }
            catch (Exception e)
            {
                return Problem(detail: e.Message);
            }
        }

        [HttpGet]
        [Route("/api/ProjectPeriodCounter")]
        public async Task<ActionResult<List<ProjectPeriodCounter>>> GetProjectPeriodCounterAsync()
        {
            try
            {
                List<ProjectPeriodCounter> projectperiodcounters = await cpcs.GetProjectPeriodCounterAsync();
                return Ok(projectperiodcounters);
            }
            catch (Exception e)
            {
                return Problem(detail: e.Message);
            }
        }

        [HttpGet]
        [Route("/api/FileSizeDivisionCounter")]
        public async Task<ActionResult<List<FileSizeDivisionCounter>>> GetFileSizeDivisionCounterAsync()
        {
            try
            {
                List<FileSizeDivisionCounter> projectlist = await cpcs.GetFileSizeDivisionCounterAsync();
                var projects = projectlist
                .OrderBy(x => x.Division)
                .ToList();
                return Ok(projects);
            }
            catch (Exception e)
            {
                return Problem(detail: e.Message);
            }
        }


    }
}
