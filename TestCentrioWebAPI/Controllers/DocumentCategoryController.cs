﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCentrioWebAPI.Models;
using TestCentrioWebAPI.Services;

namespace TestCentrioWebAPI.Controllers
{
    [ApiController]
    public class DocumentCategoryCounterController : ControllerBase
    {
        private DocumentCategoryCounterService dcs { get; set; }

        public DocumentCategoryCounterController(IConfiguration configuration)
        {
            dcs = new DocumentCategoryCounterService(configuration);
        }

        [HttpGet]
        [Route("/api/DocumentCategoryCounter")]
        public async Task<ActionResult<List<DocumentCategoryCounter>>> GetDocumentCategoryCounterAsync()
        {
            try
            {
                List<DocumentCategoryCounter> documentCategories = await dcs.GetDocumentCategoryCounterAsync();
                List<DocumentCategoryCounter> documentcategories = documentCategories.GetRange(0,10);
                return Ok(documentcategories);
            }
            catch (Exception e)
            {
                return Problem(detail: e.Message);
            }
        }
        [HttpGet]
        [Route("/api/DocumentSystemCounter")]
        public async Task<ActionResult<List<DocumentSystemCounter>>> GetDocumentSystemCounterAsync()
        {
            try
            {
                List<DocumentSystemCounter> documentCategories = await dcs.GetDocumentSystemCounterAsync();
                return Ok(documentCategories);
            }
            catch (Exception e)
            {
                return Problem(detail: e.Message);
            }
        }


    }
}
