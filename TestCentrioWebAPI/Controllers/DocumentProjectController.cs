﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCentrioWebAPI.Models;
using TestCentrioWebAPI.Services;

namespace TestCentrioWebAPI.Controllers
{
    [ApiController]
    public class DocumentProjectController : ControllerBase
    {
        private DocumentProjectService dps { get; set; }
        private DocumentClassCounterService dccs { get; set; }
        private ConstructionProjectPeriodCounterService cpcs { get; set; }
        private DocumentCategoryCounterService dcs { get; set; }


        public DocumentProjectController(IConfiguration configuration)
        {
            dps = new DocumentProjectService(configuration);
            dccs = new DocumentClassCounterService(configuration);
            cpcs = new ConstructionProjectPeriodCounterService(configuration);
            dcs = new DocumentCategoryCounterService(configuration);
        }

        [HttpGet]
        [Route("/api/DocumentProject")]
        public async Task<ActionResult<List<DocumentProject>>> GetAsync()
        {
            try
            {
                List<DocumentProject> documents = await dps.GetAsync();
                return Ok(documents);
            }
            catch (Exception e)
            {
                return Problem(detail: e.Message);
            }
        }

        [HttpGet]
        [Route("/api/DocumentProjectExtended")]
        public async Task<ActionResult<CentrioProjectExtended>> GetExtendedAsync()
        {

            try
            {
               List<DocumentCategoryCounter> documentCategories = await dcs.GetDocumentCategoryCounterAsync();
                List<DocumentCategoryCounter> documentcategories = documentCategories.GetRange(0,10);
                var result = new CentrioProjectExtended
                {
                    DocumentCompanyCounter = await dccs.GetDocumentCompanyAsync(),
                    ConstructionProjectPeriodCounter = await cpcs.GetConstructionProjectPeriodCounterAsync(),
                    ProjectPeriodCounter = await cpcs.GetProjectPeriodCounterAsync(),
                    DocumentSystemCounter = await dcs.GetDocumentSystemCounterAsync(),
                    DocumentBusinessAreaCounter = await dccs.GetDocumentBusinessAreaAsync(),
                    DocumentProjectPeriodCounter = await dccs.GetDocumentProjectPeriodAsync(),
                    FileSizeDivisionCounter = await cpcs.GetFileSizeDivisionCounterAsync(),
                    DocumentPeriodCounter = await dccs.GetDocumentPeriodAsync(),
                    DocumentCategoryCounter =  documentcategories,


                };

                return Ok(result);


            }
            catch (Exception e)
            {
                return Problem(detail: e.Message);
            }
        }

    }
}
