﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCentrioWebAPI.Models
{
    public class DocumentCompanyCounter
    {
        public string CompanyCode { get; set; } = string.Empty;
         public int Counter { get; set; } = 0;
    }
}
