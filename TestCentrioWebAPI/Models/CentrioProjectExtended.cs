﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCentrioWebAPI.Models
{
    public class CentrioProject
    {
        public long SysObjectId { get; set; } = 0;
        public string ProjectNumber { get; set; } = string.Empty;
        public string ProjectName { get; set; } = string.Empty;
    }
    public class CentrioProjectExtended
    {
        public List<ProjectPeriodCounter> ProjectPeriodCounter { get; set; } = new List<ProjectPeriodCounter>();
        public List<DocumentProjectPeriodCounter> DocumentProjectPeriodCounter { get; set; } = new List<DocumentProjectPeriodCounter>();
        public List<DocumentBusinessAreaCounter> DocumentBusinessAreaCounter { get; set; } = new List<DocumentBusinessAreaCounter>();
        public List<DocumentCompanyCounter> DocumentCompanyCounter { get; set; } = new List<DocumentCompanyCounter>();
        public List<DocumentCategoryCounter> DocumentCategoryCounter { get; set; } = new List<DocumentCategoryCounter>();
        

        public List<DocumentSystemCounter> DocumentSystemCounter { get; set; } = new List<DocumentSystemCounter>();
        public List<FileSizeDivisionCounter> FileSizeDivisionCounter { get; set; } = new List<FileSizeDivisionCounter>();

        public List<ConstructionProjectPeriodCounter> ConstructionProjectPeriodCounter { get; set; } = new List<ConstructionProjectPeriodCounter>();
        //public List<DocumentProject> DocumentProject { get; set; } = new List<DocumentProject>();
        public List<DocumentPeriodCounter> DocumentPeriodCounter { get; set; } = new List<DocumentPeriodCounter>();




    }
}
