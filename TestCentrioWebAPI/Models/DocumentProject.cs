﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCentrioWebAPI.Models
{
    public class DocumentProject
    {
        public long DocumentID { get; set; } = 0;
        public long ParentID { get; set; } = 0;
        public string ProjektNr { get; set; } = string.Empty;
        public string SAPBukrs { get; set; } = string.Empty;
        public string SAPGsber { get; set; } = string.Empty;
        public string GsberName { get; set; } = string.Empty;
        public string Sparte { get; set; } = string.Empty;
        public string Projektkategorie { get; set; } = string.Empty;
        public string Segment { get; set; } = string.Empty;
        public string Branche { get; set; } = string.Empty;
        public string Auftragsart { get; set; } = string.Empty;
        public string Abrechnungsart { get; set; } = string.Empty;
        public string ProjektTyp { get; set; } = string.Empty;
        public string Periode { get; set; } = "000000";
        public long DateiGroesse { get; set; } = 0;
    }
}
