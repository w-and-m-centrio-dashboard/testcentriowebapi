﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCentrioWebAPI.Models
{
    public class DocumentSystemCounter
    {
      
        public string Category { get; set; } = string.Empty;
        public int Counter { get; set; } = 0;
    }
}
